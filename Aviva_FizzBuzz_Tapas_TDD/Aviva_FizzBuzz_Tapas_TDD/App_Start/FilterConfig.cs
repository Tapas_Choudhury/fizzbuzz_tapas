﻿using System.Web;
using System.Web.Mvc;

namespace Aviva_FizzBuzz_Tapas_TDD
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
