﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Aviva_FizzBuzz_Tapas_TDD.BL;
using System.Drawing;


namespace Aviva_FizzBuzz_Tapas_TDD.ViewModel
{
    public class FizzBuzzViewModel : IFizzBuzzViewModel
    {
        [Required]
        [RegularExpression(@"^[1-9]\d{0,2}$", ErrorMessage = "Please enter a valid maximum no.:1-999")]
        public int MaxNumber { get; set; }
        public string DisplayNumber { get; set; }
        public List<string> FizzBuzzNumbers { get; set; }
       
        IFizzBuzzBL FizzBuzzBL;

        public FizzBuzzViewModel(IFizzBuzzBL fizzBuzzBL)
        {
            FizzBuzzNumbers = new List<string>();
            FizzBuzzBL = fizzBuzzBL;
        }

        public void GetFizzBuzzNumbers()
        {
            this.FizzBuzzNumbers = FizzBuzzBL.GetFizzBuzzNumbers(MaxNumber);
        }
    }

    public interface IFizzBuzzViewModel
    {
        int MaxNumber { get; set; }
        List<string> FizzBuzzNumbers { get; set; }
        void GetFizzBuzzNumbers();
    }
}