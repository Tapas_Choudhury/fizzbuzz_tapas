﻿
namespace Aviva_FizzBuzz_Tapas_TDD.DAL
{
    /// <summary>
    /// Considerring this as third party service/Data Access Layer. Will be mocked in the UT
    /// </summary>
    public class FizzBuzzDAL : IFizzBuzzDAL
    {
        public virtual string FizzTemplate { get; set; }
        public virtual string BuzzTemplate { get; set; }

        public FizzBuzzDAL()
        {
            FizzTemplate = "{0}izz";
            BuzzTemplate = "{0}uzz";
        }
    }

    public interface IFizzBuzzDAL
    {
        string FizzTemplate { get; set; }
        string BuzzTemplate { get; set; }
    }
}