﻿using System;
using System.Collections.Generic;

using Aviva_FizzBuzz_Tapas_TDD.DAL;
namespace Aviva_FizzBuzz_Tapas_TDD.BL
{
   
    public class FizzBuzzBL : IFizzBuzzBL
    {
        public IFizzBuzzDAL FizzBuzzData { get; set; }
        public FizzBuzzBL(IFizzBuzzDAL data)
        {
            FizzBuzzData = data;            
        }

        public List<string> GetFizzBuzzNumbers(int maxNumber)
        {
            List<string> fizzbuzzNumbers = new List<string>();
            for (int i = 1; i <= maxNumber; i++)
            {
                string fizzbuzzNumber = string.Empty;
                if (i % 3 == 0 && i % 5 == 0)
                    fizzbuzzNumber = string.Format(FizzBuzzData.FizzTemplate, DateTime.Today.DayOfWeek.ToString().Substring(0, 1)) + string.Format(FizzBuzzData.BuzzTemplate, DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
                else if (i % 3 == 0)
                    fizzbuzzNumber = string.Format(FizzBuzzData.FizzTemplate, DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
                else if (i % 5 == 0)
                    fizzbuzzNumber = string.Format(FizzBuzzData.BuzzTemplate, DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
                else
                    fizzbuzzNumber = i.ToString();

                fizzbuzzNumbers.Add(fizzbuzzNumber);

            }
            return fizzbuzzNumbers;
        }
    }

    public interface IFizzBuzzBL
    {
        IFizzBuzzDAL FizzBuzzData { get; set; }
        List<string> GetFizzBuzzNumbers(int maxNumber);
    }
}