﻿using Moq;
using NUnit.Framework;
using System.Linq;
using System.Web.Mvc;

using Aviva_FizzBuzz_Tapas_TDD.BL;
using Aviva_FizzBuzz_Tapas_TDD.Controllers;
using Aviva_FizzBuzz_Tapas_TDD.DAL;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;

namespace Aviva_FizzBuzz_Tapas_TDD.Tests.Controllers
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        [Test]
        public void Get_Show_View_Name()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("{0}izz");
            mock.Setup(q => q.BuzzTemplate).Returns("{0}uzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Show() as ViewResult;
            

            //Assert
            Assert.AreEqual("Show",result.ViewName);
        }

        [Test]
        public void Give_4_Get_12Fizz4()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("{0}izz");
            mock.Setup(q => q.BuzzTemplate).Returns("{0}uzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("4") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Mizz4", string.Concat(model.FizzBuzzNumbers.Select(q => q)));
        }

        [Test]
        public void Give_5_Get_12Fizz4Buzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("{0}izz");
            mock.Setup(q => q.BuzzTemplate).Returns("{0}uzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("5") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Mizz4Muzz", string.Concat(model.FizzBuzzNumbers.Select(q => q)));
        }

        [Test]
        public void Give_15_Get_FizzBuzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("{0}izz");
            mock.Setup(q => q.BuzzTemplate).Returns("{0}uzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("15") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Mizz4MuzzMizz78MizzMuzz11Mizz1314MizzMuzz", string.Concat(model.FizzBuzzNumbers.Select(q => q)));
        }
    }
}
